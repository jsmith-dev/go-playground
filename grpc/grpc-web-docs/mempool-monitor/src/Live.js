import React, { PureComponent } from 'react';
import * as pb from './bchrpc/bchrpc_pb_service'
import {GetMempoolInfoRequest, 
        GetBlockchainInfoRequest, 
        SubscribeTransactionsRequest,
        TransactionFilter,
        TransactionNotification,
        SubscribeBlocksRequest,
        BlockNotification
      } from './bchrpc/bchrpc_pb';

import {Row, Col} from 'react-materialize'
import { BrowserHeaders } from 'browser-headers';

// const client = new pb.bchrpcClient("https://bchd.greyh.at:8335")
// const client = new pb.bchrpcClient("https://bchd.imaginary.cash:8335")

const client = new pb.bchrpcClient("https://bchd.sploit.cash")

const headers = new BrowserHeaders();
headers.set("Access-Control-Allow-Origin", "*")
headers.set("Access-Control-Allow-Credentials", "true")
headers.set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization, x-id, Content-Length, X-Requested-With, Date , Vary, Access-Control-Allow-Origin, Access-Control-Allow-Credentials")
headers.set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")

class Live extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {  
        mempoolCount: 0,
        mempoolBytes: 0,
        blockHeight: 0,
        server: localStorage.getItem('server'),
    };
    this.getMempoolInfo = this.getMempoolInfo.bind(this);
    this.getChainInfo = this.getChainInfo.bind(this);
    this.subscribeMempool = this.subscribeMempool.bind(this);
    this.subscribeBlockChain = this.subscribeBlockChain.bind(this);

  }

  async componentDidMount() {
    await this.getMempoolInfo();
    await this.getChainInfo();
    await this.subscribeMempool();
    await this.subscribeBlockChain();
  }
  
  async getMempoolInfo() {
    const getMempoolInfoRequest = new GetMempoolInfoRequest();

    await client.getMempoolInfo(getMempoolInfoRequest, headers, function(error, responseMessage) {
      if (error) {
        console.log("Error: " + error.code + ": " + error.message)
      } else {
        var mempool = responseMessage.toObject()
        console.log(mempool)
        this.setState({
          mempoolCount: this.state.mempoolCount + mempool.size,
          mempoolBytes: this.state.mempoolBytes + mempool.bytes
        })
      }
    }.bind(this));
  }

  async getChainInfo() {
    const getBlockchainInfoRequest = new GetBlockchainInfoRequest();

    await client.getBlockchainInfo(getBlockchainInfoRequest, headers, function(error, responseMessage) {
      if (error) {
        console.log("Error: " + error.code + ": " + error.message)
      } else {
        var chain = responseMessage.toObject()
        console.log(chain)
        this.setState({blockHeight: chain.bestHeight})
      }
    }.bind(this));
  }

  async subscribeMempool() {
    var filter = new TransactionFilter();
    filter.setAllTransactions(true)

    var subscribreTransactionRequest = new SubscribeTransactionsRequest();
    subscribreTransactionRequest.setIncludeMempool(true)
    subscribreTransactionRequest.setSubscribe(filter)

    var stream = await client.subscribeTransactions(subscribreTransactionRequest, headers)

    stream.on('data', function(message) {
      var tx = new TransactionNotification()
      tx = message.toObject()
      console.log(tx.unconfirmedTransaction.transaction.size)

      this.setState({
        mempoolCount: this.state.mempoolCount + 1,
        mempoolBytes: this.state.mempoolBytes + tx.unconfirmedTransaction.transaction.size
        })

    }.bind(this));
    stream.on('status', function(status) {
      console.log(status)
    });
    stream.on('end', function(status) {
      console.log(status)
    });
  }

  async subscribeBlockChain() {
    const subscribreBlocksRequest = new SubscribeBlocksRequest();

    var stream = await client.subscribeBlocks(subscribreBlocksRequest, headers)

    stream.on('data', function(message) {
      var block = new BlockNotification()
      block = message.toObject()
      console.log(block)

      this.setState({
        mempoolCount: 0,
        mempoolBytes: 0,
        blockHeight: block.block.height
      })
      // In case the last block didn't clear the mempool, get the current mempool state
      this.getMempoolInfo();

    }.bind(this));
    stream.on('status', function(status) {
      console.log(status)
    });
    stream.on('end', function(status) {
      console.log(status)
    });
  }

  render() {
    return (
      <div className="status-header">
          <Row>
            <Col s={4} className="black-text"><b>BCH Chain Height: </b>{this.state.blockHeight}</Col>
            <Col s={4} className="black-text"><b>Mempool TX Count: </b>{this.state.mempoolCount}</Col>
            <Col s={4} className="black-text"><b>Mempool Size: </b>{(this.state.mempoolBytes / 1024).toFixed(2)} kB</Col>
          </Row>
      </div>
    );
  }
}

export default Live;

// async getMempoolInfo() {

  //   const getMempoolInfoRequest = new GetMempoolInfoRequest();
  //   // const client = new pb.bchrpcClient("https://bchd.greyh.at:8335")
  //   // const client = new pb.bchrpcClient("https://bchd.imaginary.cash:8335")
  //   // const client = new pb.bchrpcClient("https://172.31.64.100:8335");
  //   const client = new pb.bchrpcClient("https://192.168.1.120:8335");
    
  //   await grpc.invoke(pb.bchrpc.GetMempoolInfo, {
  //     request: getMempoolInfoRequest,
  //     host: "https://bchd.imaginary.cash:8335",
  //     onMessage: (message: GetMempoolInfoResponse) => {
  //       let mempool = message.toObject()
  //       console.log(mempool)
  //       this.setState({mempoolCount: mempool.size});
  //     },
  //     onEnd: (code: grpc.Code, msg: string | undefined, trailers: grpc.Metadata) => {
  //       if (code == grpc.Code.OK) {
  //         console.log("all ok")
  //       } else {
  //         console.log("hit an error", code, msg, trailers);
  //       }
  //     }
  //   });

  // }

  // async getChainInfo() {
  //   const getBlockchainInfoRequest = new GetBlockchainInfoRequest();
  //   // const client = new pb.bchrpcClient("https://bchd.greyh.at:8335")
  //   // const client = new pb.bchrpcClient("https://bchd.imaginary.cash:8335")
  //   const client = new pb.bchrpcClient("https://192.168.1.120:8335");

  //   await grpc.invoke(pb.bchrpc.GetBlockchainInfo, {
  //     request: getBlockchainInfoRequest,
  //     host: "https://bchd.imaginary.cash:8335",
  //     onMessage: (message: GetBlockchainInfoResponse) => {
  //       let mempool = message.toObject()
  //       console.log(mempool)
  //       this.setState({blockHeight: mempool.bestHeight});
  //     },
  //     onEnd: (code: grpc.Code, msg: string | undefined, trailers: grpc.Metadata) => {
  //       if (code == grpc.Code.OK) {
  //         console.log("all ok")
  //       } else {
  //         console.log("hit an error", code, msg, trailers);
  //       }
  //     }
  //   });
  // }
