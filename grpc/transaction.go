package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"path/filepath"

	pb "github.com/gcash/bchd/bchrpc/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"github.com/gcash/bchutil"
)

var certificateFile = filepath.Join(bchutil.AppDataDir("bchd", false), "bcash.crt")

func initGRPC() (*grpc.ClientConn, error) {
	creds, err := credentials.NewClientTLSFromFile(certificateFile, "localhost")
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}
	conn, err := grpc.Dial("bchd.imaginary.cash:8335", grpc.WithTransportCredentials(creds), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(33554432)))
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	return conn, nil
}

func main() {

	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	hash, err := hex.DecodeString("13d72070440d465146dbb1c1be79ce3b1c2252d3c1434a49eb53960613d41010")
	if err != nil {
		fmt.Println(err)
	}
	hash = reverse(hash)

	txResp, err := c.GetTransaction(context.Background(), &pb.GetTransactionRequest{Hash: hash})
	if err != nil {
		fmt.Println(err)
	}

	pretty, _ := json.MarshalIndent(txResp, "", "    ")
	fmt.Println(string(pretty))

	// txRawResp, err := c.GetRawTransaction(context.Background(), &pb.GetRawTransactionRequest{Hash: hash})
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// pretty, _ := json.MarshalIndent(txRawResp, "", "    ")
	// fmt.Println(string(pretty))

	// tx := txRawResp.Transaction

	// fmt.Println(tx)

	// hex := hex.EncodeToString(tx)

	// fmt.Println(hex)

}

func reverse(slice []byte) []byte {
	for i := len(slice)/2 - 1; i >= 0; i-- {
		opp := len(slice) - 1 - i
		slice[i], slice[opp] = slice[opp], slice[i]
	}

	return slice
}
