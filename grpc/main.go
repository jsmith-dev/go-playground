package main

import (
	"encoding/hex"
	"errors"
	"fmt"
	"path/filepath"
	"time"

	pb "github.com/gcash/bchd/bchrpc/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"github.com/gcash/bchutil"
)

// ShuffleTxDetails ...
type ShuffleTxDetails struct {
	TxID        string
	BlockHeight int32
	BlockHash   string
	BlockTime   int64
	Date        string
	TotalIn     int64
	TotalOut    int64
	MinUTXO     int64
	OutputCount int
	PoolSize    int
	PoolName    string
}

var certificateFile = filepath.Join(bchutil.AppDataDir("bchd", false), "bcash.crt")

func initGRPC() (*grpc.ClientConn, error) {
	creds, err := credentials.NewClientTLSFromFile(certificateFile, "localhost")
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}
	conn, err := grpc.Dial("bchd.imaginary.cash:8335", grpc.WithTransportCredentials(creds), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(33554432)))
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	return conn, nil
}

func getPoolName(minUTXO int64) (string, error) {
	if minUTXO > 10000 && minUTXO <= 100000 {
		return "0.0001", nil
	} else if minUTXO > 100000 && minUTXO <= 1000000 {
		return "0.001", nil
	} else if minUTXO > 1000000 && minUTXO <= 10000000 {
		return "0.01", nil
	} else if minUTXO > 10000000 && minUTXO <= 100000000 {
		return "0.1", nil
	} else if minUTXO > 100000000 && minUTXO <= 1000000000 {
		return "1", nil
	} else if minUTXO > 1000000000 && minUTXO <= 10000000000 {
		return "10", nil
	} else if minUTXO > 10000000000 && minUTXO <= 100000000000 {
		return "100", nil
	}

	return "", errors.New("ERROR: UTXO does not fit within pool contraints:" + string(minUTXO))
}

// ShuffleDetails ...
func ShuffleDetails(tx *pb.Transaction) (*ShuffleTxDetails, error) {

	var details ShuffleTxDetails

	identicalOutputs := make(map[int64]int)
	for _, output := range tx.Outputs {
		identicalOutputs[output.Value]++
	}

	for outputValue := range identicalOutputs {
		inputCount := len(tx.Inputs)
		outputCount := len(tx.Outputs)
		minUTXO := outputValue
		if identicalOutputs[outputValue] >= 3 && identicalOutputs[outputValue] == inputCount && outputCount >= inputCount && outputCount <= (inputCount*2)-1 && minUTXO >= 10270 {
			details.MinUTXO = minUTXO
		}
	}

	if details.MinUTXO == 0 {
		return nil, errors.New("gobitbox.ShuffleDetails: ERROR not a CashHuffle tx. txid: " + tx.String())
	}

	poolName, err := getPoolName(details.MinUTXO)
	if err != nil {
		return nil, err
	}
	details.PoolName = poolName
	tm := time.Unix(tx.Timestamp, 0)
	date := tm.Format("2006-01-02")
	details.Date = date

	details.TxID = hashString(reverse(tx.GetHash()))
	details.BlockHash = hashString(reverse(tx.GetBlockHash()))
	details.BlockHeight = tx.GetBlockHeight()
	details.BlockTime = tx.Timestamp

	var totalIn int64
	for _, in := range tx.GetInputs() {
		totalIn += in.Value
	}
	var totalOut int64
	for _, out := range tx.GetOutputs() {
		totalOut += out.Value
	}

	details.TotalIn = totalIn
	details.TotalOut = totalOut
	details.PoolSize = len(tx.Inputs)
	details.OutputCount = len(tx.Outputs)

	return &details, nil
}

// IsShuffleTX ...
func IsShuffleTX(tx *pb.Transaction) bool {
	isShuffle := false
	identicalOutputs := make(map[int64]int)

	for _, output := range tx.Outputs {
		identicalOutputs[output.Value]++
	}

	inputCount := len(tx.Inputs)
	//fmt.Println("len(tx.Inputs)", len(tx.Inputs))
	outputCount := len(tx.Outputs)

	for key := range identicalOutputs {
		//fl, _ := strconv.ParseFloat(key, 64)
		minUTXO := key
		if identicalOutputs[key] >= 3 && identicalOutputs[key] == inputCount && outputCount >= inputCount && outputCount <= (inputCount*2)-1 && minUTXO >= 10270 {
			//fmt.Println("identicalOutputs[key] == inputCount", identicalOutputs[key], inputCount)
			isShuffle = true
			break
		}
	}

	return isShuffle
}

func grpcBestBlockInfo() (int32, string, error) {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	blockchainInfoResp, err := c.GetBlockchainInfo(context.Background(), &pb.GetBlockchainInfoRequest{})
	if err != nil {

		return 0, "", err
	}

	return blockchainInfoResp.BestHeight, hashString(reverse(blockchainInfoResp.BestBlockHash)), nil
}

func main() {

	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	bestHeight, bestHash, err := grpcBestBlockInfo()
	if err != nil {
		fmt.Println(err)
		return
	}

	// bestHash, err := chainhash.NewHash(bHash)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	fmt.Println("Blockchain Height: ", bestHeight)
	fmt.Printf("Best Block Hash  : %x\n", bestHash)
	fmt.Printf("Best Block Hash  : %s\n", bestHash)
	fmt.Println("PARSING 10 LATEST BLOCKS")

	for i := int(bestHeight) - 10; i <= int(bestHeight); i++ {
		blockResp, err := c.GetBlock(context.Background(), &pb.GetBlockRequest{FullTransactions: true, HashOrHeight: &pb.GetBlockRequest_Height{Height: int32(i)}})
		if err != nil {
			fmt.Println(err)
			return
		}

		blockHash := reverse(blockResp.Block.Info.GetHash())

		fmt.Printf("NEW BLOCK: %d - TX Count: %10d - HASH: %x\n", i, len(blockResp.Block.TransactionData), blockHash)

		for _, data := range blockResp.Block.TransactionData {
			tx := data.GetTransaction()
			// fmt.Println("TX :", hash)

			if IsShuffleTX(tx) == true {
				// fmt.Println("TX :",  hash)
				// fmt.Printf("TX : %d IN : %d OUT => %x\n", len(tx.Inputs), len(tx.Outputs), hash)
				// fmt.Printf("     => %d %x %d\n", tx.BlockHeight, blockHash, blockResp.Block.Info.Timestamp)
				// for i := len(hash)/2 - 1; i >= 0; i-- {
				// 	opp := len(hash) - 1 - i
				// 	hash[i], hash[opp] = hash[opp], hash[i]
				// }
				// fmt.Println("TX :", hash)
				// for _, input := range tx.Inputs {
				// 	fmt.Println("IN :", input.Address, input.Value, input.Index)
				// }
				// for _, out := range tx.Outputs {
				// 	fmt.Println("OUT:", out.Address, out.Value, out.Index)
				// }

				details, err := ShuffleDetails(tx)
				if err != nil {
					fmt.Println(err)
				}

				fmt.Printf("TX : %d IN : %d OUT => %s\n", details.PoolSize, details.OutputCount, details.TxID)
				fmt.Printf("     => %d %s %d\n", details.BlockHeight, details.BlockHash, details.BlockTime)
				// fmt.Printf("%+v\n", details)
			}

		}

	}

}

func hashString(hash []byte) string {
	hashString := hex.EncodeToString(hash)

	return hashString
}

func reverse(slice []byte) []byte {
	for i := len(slice)/2 - 1; i >= 0; i-- {
		opp := len(slice) - 1 - i
		slice[i], slice[opp] = slice[opp], slice[i]
	}

	return slice
}
