package main

import (
	"fmt"
	//"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	//"github.com/btcsuite/btcutil"
	"github.com/btcsuite/btcutil/hdkeychain"
)

func main() {
	// Generate a random seed at the recommended length.
	seed, err := hdkeychain.GenerateSeed(hdkeychain.RecommendedSeedLen)
	//seed, err := hdkeychain.GenerateSeed(256)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("seed", seed)

	// Generate a new master node using the seed.
	masterKey, err := hdkeychain.NewMaster(seed, &chaincfg.MainNetParams)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("master", masterKey)

	// Show that the generated master node extended key is private.
	fmt.Println("Private Extended Key?:", masterKey.IsPrivate())

	purpose, err := masterKey.Child(hdkeychain.HardenedKeyStart + 44)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("purpose", purpose)
	fmt.Println("Private Extended Key?:", purpose.IsPrivate())

	coin, err := purpose.Child(hdkeychain.HardenedKeyStart + 145)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("coin", coin)
	fmt.Println("Private Extended Key?:", coin.IsPrivate())
}
