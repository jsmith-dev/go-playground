package main

import (
	"fmt"
	"github.com/WeMeetAgain/go-hdwallet"
)

func main() {
	// Generate a random 256 bit seed
	seed, err := hdwallet.GenSeed(256)
	if err != nil {
		//
	}
	fmt.Println("seed:", seed)

	// Create a master private key
	masterprv := hdwallet.MasterKey(seed)
	fmt.Println("mprv:", masterprv)

	// Convert a private key to public key
	masterpub := masterprv.Pub()
	fmt.Println("mpub:", masterpub)

	// Generate new child key based on private or public key
	childprv, err := masterprv.Child(145)
	if err != nil {
		//
	}
	fmt.Println("cprv:", childprv)

	childpub, err := masterpub.Child(145)
	if err != nil {
		//
	}
	fmt.Println("cpub:", childpub)

	// Create bitcoin address from public key
	address := childpub.Address()
	fmt.Println("cpuba:", address)

	// Convenience string -> string Child and ToAddress functions
	walletstring := childpub.String()
	fmt.Println("walletstring", walletstring)
	childstring, err := hdwallet.StringChild(walletstring, 0)
	if err != nil {
		//
	}
	fmt.Println("childstring:", childstring)
	childaddress, err := hdwallet.StringAddress(childstring)
	if err != nil {
		//
	}
	fmt.Println("childaddress:", childaddress)
}
