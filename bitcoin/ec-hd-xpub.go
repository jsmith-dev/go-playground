// EC
//seed: company gesture age since woman panel silly victory funny coffee dog strike
//xpub: xpub661MyMwAqRbcF5bTJyG1jcbceQU28cLRYd94tKzRuAS6NfuA3ga39GnRBsPXiGuw3THNfUtEMiSQW8ApD4nzJDvPzJ9v2k1SfC4KHsyDeHu
// 0 - 1PLs5aaBgS5SjQWM2TTVUb53nCtb6oH9wE
// 1 - 1B8QXT3VUxxHpxW6kvdH96ARrJGarvXWrN
// 2 - 19J7iP3cd3Rs5DHEcPWhU5eK5LgDP5RemK

package main

import (
	"fmt"
	//"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil"
	"github.com/btcsuite/btcutil/hdkeychain"
)


func main() {
	xpub := "xpub661MyMwAqRbcF5bTJyG1jcbceQU28cLRYd94tKzRuAS6NfuA3ga39GnRBsPXiGuw3THNfUtEMiSQW8ApD4nzJDvPzJ9v2k1SfC4KHsyDeHu"

	var chain uint32
	for chain=0; chain < 2; chain++ {
		var index uint32
		for index=0; index < 10; index++ {
			addr, err := genAddressByIndex(xpub, chain, index)
			if err != nil {
			    fmt.Println(err)
			    return
			}
			fmt.Println("chain:", chain, "addr:", index, addr)
		}
	}
}

func genAddressByIndex(xpub string, chain uint32, index uint32) (*btcutil.AddressPubKeyHash, error) {
	xpubKey, err := hdkeychain.NewKeyFromString(xpub)
	if err != nil {
	    fmt.Println(err)
	    return nil, err
	}

	//fmt.Println("master xpub:", xpubKey)
	//fmt.Println("Public Extended Key?:", !xpubKey.IsPrivate())

	acctKey, err := xpubKey.Child(chain)
	if err != nil {
	    fmt.Println(err)
	    return nil, err
	}

	//fmt.Println("account xpub", acctKey)

	chainKey, err := acctKey.Child(index)
	if err != nil {
	    fmt.Println(err)
	    return nil, err
	}

	//fmt.Println("chain xpub", chainKey)

	addr, err := chainKey.Address(&chaincfg.MainNetParams)
	if err != nil {
	    fmt.Println(err)
	    return nil, err
	}

	//fmt.Println(acct0ExtKeyAddr)

	return addr, nil
}