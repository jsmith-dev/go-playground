package main

import (
	//	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"strconv"
	"github.com/bchsuite/bchd/bchec"
	"github.com/bchsuite/bchd/chaincfg"
	"github.com/bchsuite/bchutil"
	//"github.com/bchsuite/bchutil/hdkeychain"
)

type Network struct {
	name        string
	symbol      string
	xpubkey     byte
	xprivatekey byte
}

var network = map[string]Network{
	"btc": {name: "bitcoin", symbol: "btc", xpubkey: 0x00, xprivatekey: 0x80},
	"bch": {name: "bitcoincash", symbol: "bch", xpubkey: 0x00, xprivatekey: 0x80},
}

func (network Network) GetNetworkParams() *chaincfg.Params {
	networkParams := &chaincfg.MainNetParams
	networkParams.PubKeyHashAddrID = network.xpubkey
	networkParams.PrivateKeyID = network.xprivatekey
	return networkParams
}

func (network Network) CreatePrivateKey() (*bchutil.WIF, error) {
	secret, err := bchec.NewPrivateKey(bchec.S256())
	if err != nil {
		return nil, err
	}
	return bchutil.NewWIF(secret, network.GetNetworkParams(), true)
}

//func (network Network) ImportPrivateKey(secretHex string) (*bchutil.WIF, error) {}

func (network Network) ImportWIF(wifStr string) (*bchutil.WIF, error) {
	wif, err := bchutil.DecodeWIF(wifStr)
	if err != nil {
		return nil, err
	}
	if !wif.IsForNet(network.GetNetworkParams()) {
		return nil, errors.New("The WIF string is not valid for the `" + network.name + "` network")
	}
	return wif, nil
}

func (network Network) GetAddress(wif *bchutil.WIF) (*bchutil.AddressPubKey, error) {
	return bchutil.NewAddressPubKey(wif.PrivKey.PubKey().SerializeCompressed(), network.GetNetworkParams())
}

func main() {
	//var err error
	if len(os.Args) < 2 {
		fmt.Println("Specify amount of addresses to generate: gen_wif_addr <amount>")
		return
	}
	arg := os.Args[1]
	amount, err := strconv.Atoi(arg)
	if err != nil {
		fmt.Println(err)
	}
	if amount <= 0 {
		fmt.Println("Specify amount of addresses to generate: gen_wif_addr <amount>")
		return
	}
	fmt.Printf("Generating %d  Bitcoin keypairs\n", amount)
	for i := 0; i < amount; i++ {
		wif, _ := network["bch"].CreatePrivateKey()
		address, _ := network["bch"].GetAddress(wif)
		fmt.Printf("%5d: %s - %s\n", i+1, wif.String(), address.EncodeAddress())
	}
}
