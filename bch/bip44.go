// Generate seed, xpriv and xpub key that can be imported into electron-cash.
// According to the BIP32 / BIP39 / BIP44 spec.
//
// - BIP39 128bit entropy mnemonic -> seed
// - BIP32 HD wallet
// - BIP44 m/44H/145H/0H standard Bitoin Cash derivation path
//
// IMPORT INTO EC: std wallet > seed > tick BIP39 > derivation path m/44'/145'/0'
//
// Expected result (actual values will differ).
//
// mnemonic: tattoo purpose retreat project flag divide steak deputy toddler dash inmate humble
// seed: [32 133 151 199 4 158 56 176 232 46 6 182 225 239 68 39 92 197 69 142 232 142 52 245 42 12 164 55 154 226 135 7 151 194 190 29 149 46 177 221 94 205 67 18 218 55 93 194 197 152 173 232 29 155 47 251 19 230 152 153 183 82 55 65]
// path: [44 145 0]
// xpriv: xprv9ydUp6yJXnsprefirakwv9BRsw68hyGEmgiHxtCeRfwML8H6qZZsL5iUSyYKjrk9ByY7m9s1hRD69V2YtFmzYyFeuMADiCAmSCxCEbJVj3B
//  xpub: xpub6CcqDcWCNAS858kBxcHxHH8ARxvd7Rz68udtmGcFz1ULCvcFP6t7st2xJGLKGqAacMpBAkc4LzDEnKdhzy3tCyKhH5BQL5ptpdU899FD3DS
// External chain: 0
// chain: 0 addr: 0 1DzvZii7q6hd9foVQH8q5jmgGf8rka75GB
// chain: 0 addr: 1 1Nugd7spH2Qx5otY2Gg288sHh4YNUkVeUX
// chain: 0 addr: 2 18UCGpTEK9ftnLgaNn3YCR6FBqBH3vJRkm
// chain: 0 addr: 3 1HcVG5Xk6gkUHeqRhWZtsHaJ4DzideSdxD
// chain: 0 addr: 4 1AKATg8A7xUW6fbiDvWzLZn9e9fAFaqHCu
// chain: 0 addr: 5 1GrQBGu2hcJCfyvyiruNWytGPxZQ5Ns8he
// chain: 0 addr: 6 1NfdKc6NFj7c9HJSJbPFPnkx3SLr76qPv6
// chain: 0 addr: 7 12vSfwh9exvLL3PDvXAqHSAbz2LYPqL4MF
// chain: 0 addr: 8 15LTUHksazqc86tf29vbZB8q9XwJdFzkXf
// chain: 0 addr: 9 1BMi2j5JWXzaSVBD3KVhTkMF4aVAyuF6nw
// Internal chain: 1
// chain: 1 addr: 0 1QEqt4ZYyyQWzM7cN6KgV98yx3pBqNpd3E
// chain: 1 addr: 1 1Ae4V3ENRChCVDATmzGFiAyo1VTd42Nrrj
// chain: 1 addr: 2 14jGQmpZxxXaPqkUrH6C8tUEuvH4EspupB
// chain: 1 addr: 3 1NRQ3qWAFoX3P4e5oFhEWp7om1BkbwUH8h
// chain: 1 addr: 4 164b14VRuAECttw58FSQeqjzQNSwPVGQZE
// chain: 1 addr: 5 1LdfV9oQ3gRovuV37KfQoau6aNzWfWuPBF
// chain: 1 addr: 6 14KQfeVwM2CcTUETrLS7Fzafw65BXYdaPd
// chain: 1 addr: 7 1MPdkSHSzBaDe7F3JaNPouqpv9hJ4qQLXr
// chain: 1 addr: 8 1Dj8yYgFjVD9wFyYUyW2ozDPqAkSKX1gPy
// chain: 1 addr: 9 1FPYxRv6yb89RgehHWBNfztzCN5QdNew2W

package main

import (
	"fmt"
	//"github.com/bchsuite/bchd/btcec"
	"github.com/bchsuite/bchd/chaincfg"
	"github.com/bchsuite/bchutil"
	"github.com/bchsuite/bchutil/hdkeychain"
	b39 "github.com/tyler-smith/go-bip39"
)

func main() {
	// Desired derivation path for xpriv and xpub m/44H/145H/0H
	path := [3]uint32{44, 145, 0}

	// Generate mnemonic
	entropy, err := b39.NewEntropy(128)
	if err != nil {
		fmt.Println(err)
		return
	}
	mnemonic, err := b39.NewMnemonic(entropy)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Fixed seed for testing
	//
	// var seed = []byte("ihasseedihasseedihasseedihasseed")

	// Generate a random seed at the recommended length.
	//
	// seed, err := hdkeychain.GenerateSeed(hdkeychain.RecommendedSeedLen)
	// if err != nil {
	//     fmt.Println(err)
	//     return
	// }

	// Generate seed from mnemonic
	seed, err := b39.NewSeedWithErrorChecking(mnemonic, "")
	if err != nil {
		fmt.Println(err)
		return
	}
	
	// Generate a wallet xpriv and xpub key from seed at m/44H/145H/0H
	xpriv, xpub, err := generateWallet(seed, path)
	if err != nil {
	    fmt.Println(err)
	    return
	}

	fmt.Println("mnemonic:", mnemonic)
	fmt.Println("seed:", seed)
	fmt.Println("path:", path)
	fmt.Printf("%6s %s\n", "xpriv:", xpriv)
	fmt.Printf("%6s %s\n", "xpub:", xpub)

	// Print first 10 addresses at chain 0 and 1 
	// m/44H/145H/0H/0/0-9
	// m/44H/145H/0H/1/0-9
	var chain uint32
	for chain=0; chain < 2; chain++ {
		if chain == 0 {
			fmt.Println("External chain:", chain)
		}
		if chain == 1 {
			fmt.Println("Internal chain:", chain)
		}

		var index uint32
		for index=0; index < 10; index++ {
			addr, err := genAddressByIndex(xpub, chain, index)
			if err != nil {
			    fmt.Println(err)
			    return
			}
			fmt.Println("chain:", chain, "addr:", index, addr)
		}
	}
}

func generateWallet(seed []byte, path [3]uint32) (*hdkeychain.ExtendedKey, *hdkeychain.ExtendedKey, error) {
	// Generate a new master node using the seed.
	// /m
	masterXpriv, err := hdkeychain.NewMaster(seed, &chaincfg.MainNetParams)
	if err != nil {
	    return nil, nil, err
	}

	xpriv := masterXpriv
	for _, node := range path {
		xpriv, err = xpriv.Child(hdkeychain.HardenedKeyStart + node)
		if err != nil {
		    return nil, nil, err
		}
		// fmt.Println(node)
	}

	xpub, err := xpriv.Neuter()
	if err != nil {
	    return nil, nil, err
	}

	return xpriv, xpub, nil
}

func genAddressByIndex(xpub *hdkeychain.ExtendedKey, chain uint32, index uint32) (*bchutil.AddressPubKeyHash, error) {
	//xpubKey, err := hdkeychain.NewKeyFromString(xpub)
	xpubKey := xpub

	chainKey, err := xpubKey.Child(chain)
	if err != nil {
	    return nil, err
	}

	addrKey, err := chainKey.Child(index)
	if err != nil {
	    return nil, err
	}

	addr, err := addrKey.Address(&chaincfg.MainNetParams)
	if err != nil {
	    return nil, err
	}

	return addr, nil
}
